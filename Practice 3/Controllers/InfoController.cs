﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace Practice_3.Controllers
{
    [ApiController]
    [Route("/api/info")]
    public class InfoController : ControllerBase
    {
        private readonly IConfiguration _config;
        public InfoController(IConfiguration config)
        {
            _config = config;
        }

        [HttpGet]
        public string GetInfo() {
            string title = _config.GetSection("Project").GetSection("Title").Value;
            string envName = _config.GetSection("EnvironmentName ").Value;
            string dbConnection = _config.GetConnectionString("Database");

            Console.WriteLine($"We are conecting to: {dbConnection}");

            return $"Project title: {title}\nEnvironment name: {envName}";
        }
    }
}
